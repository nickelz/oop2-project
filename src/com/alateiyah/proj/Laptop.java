package com.alateiyah.proj;

public class Laptop extends Computer {
	
	// Properties
	private boolean Bluetooth;
	private boolean WirelessConectivity;
	private boolean SDCardReader;
	
	// Constructor
	public Laptop(boolean Bluetooth, boolean WirelessConectivity, boolean SDCardReader, String OperatingSystem, String Processor, String GraphicsCard, String RAM, String Storage, int USBPorts) {
		super(OperatingSystem, Processor, GraphicsCard, RAM, Storage, USBPorts);
		this.Bluetooth = Bluetooth;
		this.WirelessConectivity = WirelessConectivity;
		this.SDCardReader = SDCardReader;
	}
	
	// Getter
	public boolean isBluetooth() {
		return Bluetooth;
	}

	public boolean isWirelessConectivity() {
		return WirelessConectivity;
	}

	public boolean isSDCardReader() {
		return SDCardReader;
	}
	
	// Setter
	public void setBluetooth(boolean Bluetooth) {
		this.Bluetooth = Bluetooth;
	}

	public void setWirelessConectivity(boolean WirelessConectivity) {
		this.WirelessConectivity = WirelessConectivity;
	}

	public void setSDCardReader(boolean SDCardReader) {
		this.SDCardReader = SDCardReader;
	}
	
	// toString
	@Override
	public String toString() {
		return "Laptop{" + "Bluetooth=" + Bluetooth + ", WirelessConectivity=" + WirelessConectivity + ", SDCardReader=" + SDCardReader + '}';
	}
	
}
