package com.alateiyah.proj;

public class Computer {
	
	// Properties
	private String OperatingSystem;
	private String Processor;
	private String GraphicsCard;
	private String RAM;
	private String Storage;
	private int USBPorts;
	
	// Constructor
	public Computer(String OperatingSystem, String Processor, String GraphicsCard, String RAM, String Storage, int USBPorts) {
		this.OperatingSystem = OperatingSystem;
		this.Processor = Processor;
		this.GraphicsCard = GraphicsCard;
		this.RAM = RAM;
		this.Storage = Storage;
		this.USBPorts = USBPorts;
	}
	
	// Getter
	public String getOperatingSystem() {
		return OperatingSystem;
	}

	public String getProcessor() {
		return Processor;
	}

	public String getGraphicsCard() {
		return GraphicsCard;
	}

	public String getRAM() {
		return RAM;
	}

	public String getStorage() {
		return Storage;
	}

	public int getUSBPorts() {
		return USBPorts;
	}
	
	// Setter
	public void setOperatingSystem(String OperatingSystem) {
		this.OperatingSystem = OperatingSystem;
	}

	public void setProcessor(String Processor) {
		this.Processor = Processor;
	}

	public void setGraphicsCard(String GraphicsCard) {
		this.GraphicsCard = GraphicsCard;
	}

	public void setRAM(String RAM) {
		this.RAM = RAM;
	}

	public void setStorage(String Storage) {
		this.Storage = Storage;
	}

	public void setUSBPorts(int USBPorts) {
		this.USBPorts = USBPorts;
	}

	// toString
	@Override
	public String toString() {
		return "Computer{" + "OperatingSystem=" + OperatingSystem + ", Processor=" + Processor + ", GraphicsCard=" + GraphicsCard + ", RAM=" + RAM + ", Storage=" + Storage + ", USBPorts=" + USBPorts + '}';
	}
	
}
