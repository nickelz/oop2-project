package com.alateiyah.proj;

import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 *
 * @author Ali Alateiyah
 */

public class main {
	
	public static void main(String[] args) {
		try {
			Home home = new Home();
			home.setSize(1090, 710);
			home.setUndecorated(true);
			home.setResizable(false);
			home.setLocationRelativeTo(null);
			home.setVisible(true);
			home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		} catch(Exception e) {
			System.err.println("MAIN EXCEPTION: \n" );
			e.printStackTrace();
		}
	}
	
}