package com.alateiyah.proj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 *
 * @author Ali Alateiyah
 */

public class DarkScrollPane extends BasicScrollBarUI {
    private final Dimension d = new Dimension();
	private final static String DARK_BLUE = "#34374C";
	private final static String DARK_BLUE_2 = "#2C2E3E";
	private final static String LIGHT_BLUE = "#5D6074";
	
    @Override
    protected JButton createDecreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return d;
            }
        };
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return d;
            }
        };
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
		Insets insets = c.getInsets();
        int x = insets.left;
        int y = insets.top;
        int w = c.getWidth() - insets.left - insets.right;
        int h = c.getHeight() - insets.top - insets.bottom;
		int arc = 20;
		
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setPaint(Color.decode(DARK_BLUE));
		g2.fillRoundRect(x, y, w, h, arc, arc);
		g2.drawRoundRect(x, y, w, h, arc, arc);
		g2.dispose();
	}

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Color color = null;
        JScrollBar sb = (JScrollBar) c;
        if (!sb.isEnabled() || r.width > r.height) {
            return;
        } else if (isDragging) {
            color = Color.decode(LIGHT_BLUE);
        } else if (isThumbRollover()) {
            color = Color.decode(DARK_BLUE);
        } else {
            color = Color.decode(DARK_BLUE_2);
        }
		
		int arc = 20;
		
        g2.setPaint(color);
        g2.fillRoundRect(r.x, r.y, r.width, r.height, arc, arc);
        g2.setPaint(Color.decode(LIGHT_BLUE));
        g2.drawRoundRect(r.x, r.y, r.width, r.height, arc, arc);
        g2.dispose();
    }

    @Override
    protected void setThumbBounds(int x, int y, int width, int height) {
        super.setThumbBounds(x, y, width, height);
        scrollbar.repaint();
    }
}