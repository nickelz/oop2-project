package com.alateiyah.proj;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.MatteBorder;

/**
 *
 * @author Ali Alateiyah
 */

public class JNTable {
	
	// Properties
	private ArrayList<Item> ROWList;
	public JScrollPane MainScrollPane;
	public JComponent MainPanel;
	private ButtonHandler handler;
	private final static String DARK_BLUE = "#34374C";
	private final static String DARK_BLUE_2 = "#2C2E3E";
	private final static String LIGHT_BLUE = "#5D6074";

	// Constructor
	public JNTable(JComponent mainPanel) {
		ROWList = new ArrayList<>();
		MainPanel = mainPanel;
		MainScrollPane = new JScrollPane(mainPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		MainScrollPane.setLayout(new ScrollPaneLayout());
		MainScrollPane.setPreferredSize(new Dimension(860, 637));
		MainScrollPane.getVerticalScrollBar().setBackground(Color.decode(DARK_BLUE_2));
		MainScrollPane.setBorder(BorderFactory.createEmptyBorder());
		MainScrollPane.getVerticalScrollBar().setUI(new DarkScrollPane());
	}
	
	public void addNewRow(Item item) {
		ROWList.add(item);
		MainPanel.add(item.getPanel());
	}
	
	public void removeRow(String itemName) {
		for (int i = 0; i < ROWList.size(); i++) {
			if (ROWList.get(i).nameLabel.getText().equals(itemName)) {
				ROWList.get(i).getPanel().setVisible(false);
				ROWList.remove(i);
				break;
			}
		}
	}
	
	public class Item {
		
		// Componenets
		JPanel panel;
		JLabel nameLabel;
		JLabel sellerLabel;
		JLabel desciptionLabel;
		JLabel starsLabel;
		JLabel votersLabel;
		JLabel iconLabel;

		// Constructor
		public Item(String name, String seller, String desciption, String stars, int voters, String icon) {
			this.panel = new JPanel();
			this.nameLabel = new JLabel(name);
				this.nameLabel.setBounds(90, 20, 150, 20);
				this.nameLabel.setFont(getCustomFont("Medium", 18));
				this.nameLabel.setForeground(Color.WHITE);
			this.sellerLabel = new JLabel("by " + seller);
				this.sellerLabel.setBounds(90, 45, 100, 20);
				this.sellerLabel.setFont(getCustomFont("Bold", 14));
				this.sellerLabel.setForeground(Color.decode("#6C6F86"));
			this.desciptionLabel = new JLabel(desciption);
				this.desciptionLabel.setBounds(90, 65, 750, 60);
				this.desciptionLabel.setFont(getCustomFont("Bold", 13));
				this.desciptionLabel.setForeground(Color.WHITE);
			this.starsLabel = new JLabel(getSpecificImageSize(stars, 0, 0, true));
				this.starsLabel.setBounds(610, 15, 150, 30);
			this.votersLabel = new JLabel(voters + " Voter(s)");
				this.votersLabel.setBounds(760, 15, 150, 30);
				this.votersLabel.setFont(getCustomFont("Bold", 13));
				this.votersLabel.setForeground(Color.decode("#6C6F86"));
			this.iconLabel = new JLabel(getSpecificImageSize(icon, 59, 118, false));
				this.iconLabel.setBounds(10, 10, 59, 118);
				
			// Customization
			this.panel.setLayout(null);
			this.panel.setBackground(Color.decode(DARK_BLUE_2));
			this.panel.setMaximumSize(new Dimension(1750, 150));
			this.panel.setMinimumSize(new Dimension(1750, 150));
			this.panel.setBorder(new MatteBorder(0, 0, 1, 0, Color.decode(LIGHT_BLUE)));
			this.panel.setPreferredSize(new Dimension(1000, 150));
			this.panel.add(this.nameLabel);
			this.panel.add(this.sellerLabel);
			this.panel.add(this.desciptionLabel);
			this.panel.add(this.starsLabel);
			this.panel.add(this.votersLabel);
			this.panel.add(this.iconLabel);

			// Mouse Listener
			handler = new ButtonHandler();
			this.iconLabel.addMouseListener(handler);
		}
		
		JLabel productImage;
		JLabel productSeller;
		JLabel productDeleteButton;
		JLabel productPrice;
		JLabel productQuantity;
		
		public Item(String nameLabel, String productImage, String productSeller, String productDeleteButton, String productPrice, String productQuantity) {
			this.productImage = new JLabel(getSpecificImageSize("PIcons/iPhoneX.png", 70, 130, false));
			this.productImage.setBounds(10, 0, 100, 130);
		this.nameLabel = new JLabel("Apple iPhone X");
			this.nameLabel.setForeground(Color.WHITE);
			this.nameLabel.setFont(getCustomFont("Bold", 16));
			this.nameLabel.setBounds(140, 0, 200, 50);
		this.productSeller = new JLabel("By Apple");
			this.productSeller.setForeground(Color.gray);
			this.productSeller.setFont(getCustomFont("SemiBold", 12));
			this.productSeller.setBounds(140, 25, 200, 50);
		this.productDeleteButton = new JLabel("DELETE");
			this.productDeleteButton.setForeground(Color.red);
			this.productDeleteButton.setFont(getCustomFont("SemiBold", 12));
			this.productDeleteButton.setBounds(140, 80, 200, 50);
		this.productPrice = new JLabel("$998.99");
			this.productPrice.setForeground(Color.WHITE);
			this.productPrice.setFont(getCustomFont("SemiBold", 16));
			this.productPrice.setBounds(380, 0, 200, 50);
		this.productQuantity = new JLabel(" 2 ");
			this.productQuantity.setForeground(Color.WHITE);
			this.productQuantity.setFont(getCustomFont("SemiBold", 14));
			this.productQuantity.setBounds(600, 0, 200, 50);
		}
		
		public Item(JComponent [] components) {
			this.panel = new JPanel();
			
			for (JComponent component : components) {
				this.panel.add(component);
			}
			
			// Customization
			this.panel.setLayout(null);
			this.panel.setBackground(Color.decode(DARK_BLUE_2));
			this.panel.setMaximumSize(new Dimension(1750, 150));
			this.panel.setMinimumSize(new Dimension(1750, 150));
			this.panel.setBorder(new MatteBorder(0, 0, 1, 0, Color.decode(LIGHT_BLUE)));
			this.panel.setPreferredSize(new Dimension(1000, 150));
			
		}
		private ImageIcon getSpecificImageSize(String imageString, int width, int height, boolean defaultSize) {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("Images/" + imageString));
		Image image = imageIcon.getImage();
		if (defaultSize) {
			return new ImageIcon(image);
		} else {
			Image newImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			return new ImageIcon(newImage);
		}
		}

		public JPanel getPanel() {
			return panel;
		}
		
	}
	
	// Action Listener
		private class ButtonHandler extends MouseAdapter {

			@Override
			public void mouseClicked(MouseEvent event) {
				for (Item item : ROWList) {
					if (event.getComponent().equals(item.iconLabel)) {
						
					}
				}
			}
			
		}
				
	private ImageIcon getSpecificImageSize(String imageString, int width, int height, boolean defaultSize) {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("Images/" + imageString));
		Image image = imageIcon.getImage();
		if (defaultSize) {
			return new ImageIcon(image);
		} else {
			Image newImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			return new ImageIcon(newImage);
		}
	}
	
	private Font getCustomFont(String fontName, int size) {
		try {
			return Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("Fonts/"+ fontName + ".ttf")).deriveFont(Font.PLAIN, size);
		} catch (FontFormatException | IOException ex) {
			System.err.println("FONT EXCEPTION: \n" + ex);
		}
		return new Font(null, Font.PLAIN, 8);
	}

	public ArrayList<Item> getROWList() {
		return ROWList;
	}

	public JScrollPane getMainScrollPane() {
		return MainScrollPane;
	}
	
}