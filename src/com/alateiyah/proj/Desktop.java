package com.alateiyah.proj;

public class Desktop extends Computer {
	
	// Properties
	private String CaseType;
	private int FansCapacity;
	private String SidePanelType;
	
	// Constructor
	public Desktop(String CaseType, int FansCapacity, String SidePanelType, String OperatingSystem, String Processor, String GraphicsCard, String RAM, String Storage, int USBPorts) {
		super(OperatingSystem, Processor, GraphicsCard, RAM, Storage, USBPorts);
		this.CaseType = CaseType;
		this.FansCapacity = FansCapacity;
		this.SidePanelType = SidePanelType;
	}
	
	// Getter
	public String getCaseType() {
		return CaseType;
	}

	public int getFansCapacity() {
		return FansCapacity;
	}

	public String getSidePanelType() {
		return SidePanelType;
	}
	
	// Setter
	public void setCaseType(String CaseType) {
		this.CaseType = CaseType;
	}

	public void setFansCapacity(int FansCapacity) {
		this.FansCapacity = FansCapacity;
	}

	public void setSidePanelType(String SidePanelType) {
		this.SidePanelType = SidePanelType;
	}
	
	// toString
	@Override
	public String toString() {
		return "Desktop{" + "CaseType=" + CaseType + ", FansCapacity=" + FansCapacity + ", SidePanelType=" + SidePanelType + '}';
	}
	
}
