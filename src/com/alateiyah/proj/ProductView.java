package com.alateiyah.proj;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Ali Alateiyah
 */

public class ProductView extends JFrame {
	
	// Properties
	private final static String DARK_BLUE = "#34374C";
	private final static String DARK_BLUE_2 = "#2C2E3E";
	private final static String LIGHT_BLUE = "#5D6074";

	// Components
	private static JPanel TitleContainer, RightContainer, LeftContainer, LeftTopContainer, LeftBottomContainer, RoundedPanel, MainPanel, TargetPanel;
	private static JLabel LogoIconLabel, HomeLabel, SearchLabel, CartLabel, OrdersLabel, HelpLabel, AboutLabel;
	private static JLabel ProfilePictureLabel, UsernameLabel, EditProfileLabel;
	private static JLabel HeaderTitleLabel, ExitIcon, EmptyLabel;
	private static JLabel CVCLabel, CardNumberLabel;
	private static JTextField CVCTextField, CardNumberTextField;
	private static JButton AddToCartButton, BuyNowButton;
	
	// Fonts
	private static Font _BOLD_16;
	
	public ProductView() {
		// Definition
		TitleContainer = new JPanel(new BorderLayout(100, 100));
		RightContainer = new JPanel(new BorderLayout());
			MainPanel = new JPanel(new BorderLayout());
				TargetPanel = new JPanel();
				
		RoundedPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Insets insets = getInsets();
                int x = insets.left;
                int y = insets.top;
                int w = getWidth() - insets.left - insets.right;
                int h = getHeight() - insets.top - insets.bottom;
                int arc = 25;

                Graphics2D g2 = (Graphics2D) g.create();
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setColor(Color.decode(DARK_BLUE_2));
                g2.fillRoundRect(x, y, w, h, arc, arc);
                g2.drawRoundRect(x, y, w, h, arc, arc);
                g2.dispose();
            }

        };
		LeftContainer = new JPanel(new BorderLayout());
			LeftTopContainer = new JPanel();
			LeftBottomContainer = new JPanel();
	
	_BOLD_16 = getCustomFont("Bold", 16);
		
		// Customization
		setLayout(new BorderLayout());
		getContentPane().setBackground(Color.decode(DARK_BLUE));
		
		// Title bar
		TitleContainer.setSize(1090, 30);
		TitleContainer.setBackground(Color.decode(DARK_BLUE_2));
			HeaderTitleLabel = new JLabel("HOME");
			HeaderTitleLabel.setFont(getCustomFont("Bold", 15));
			HeaderTitleLabel.setForeground(Color.WHITE);
			HeaderTitleLabel.setHorizontalAlignment(JLabel.CENTER);
			
			ExitIcon = new JLabel(getSpecificImageSize("X.png", 12, 12, false));
			ExitIcon.setPreferredSize(new Dimension(50, 20));
			
			EmptyLabel = new JLabel();
			EmptyBorder headerWhiteSpace = new EmptyBorder(0, 40, 0, 40);
			EmptyLabel.setBorder(headerWhiteSpace);
		TitleContainer.add(ExitIcon, BorderLayout.WEST);
		TitleContainer.add(HeaderTitleLabel, BorderLayout.CENTER);
		TitleContainer.add(EmptyLabel, BorderLayout.EAST);
		
		// Side menu
		LeftTopContainer.setBackground(Color.decode(DARK_BLUE));
		LeftTopContainer.setLayout(new BoxLayout(LeftTopContainer, BoxLayout.Y_AXIS));
		LeftTopContainer.setPreferredSize(new Dimension(180, 460));
		EmptyBorder leftContainerBorder = new EmptyBorder(25, 50, 0, 0);
		LeftTopContainer.setBorder(leftContainerBorder);
			LogoIconLabel = new JLabel(getSpecificImageSize("loop-logo.png", 80, 80, false));
			LogoIconLabel.setPreferredSize(new Dimension(80, 80));
			EmptyBorder logoWhiteSpace = new EmptyBorder(0, 0, 50, 0);
			LogoIconLabel.setBorder(logoWhiteSpace);
			
			HomeLabel = new JLabel("Home");
			HomeLabel.setFont(_BOLD_16);
			HomeLabel.setForeground(Color.WHITE);
			SearchLabel = new JLabel("Search");
			SearchLabel.setFont(_BOLD_16);
			SearchLabel.setForeground(Color.WHITE);
			
			CartLabel = new JLabel("Cart");
			CartLabel.setFont(_BOLD_16);
			CartLabel.setForeground(Color.WHITE);
			
			OrdersLabel = new JLabel("Orders");
			OrdersLabel.setFont(_BOLD_16);
			OrdersLabel.setForeground(Color.WHITE);
			
			
			AboutLabel = new JLabel("About");
			AboutLabel.setFont(_BOLD_16);
			AboutLabel.setForeground(Color.WHITE);
			
			HelpLabel = new JLabel("Help");
			HelpLabel.setFont(_BOLD_16);
			HelpLabel.setForeground(Color.WHITE);
		LeftTopContainer.add(LogoIconLabel);
		LeftTopContainer.add(HomeLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(SearchLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(CartLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(OrdersLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 50)));
		LeftTopContainer.add(AboutLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(HelpLabel);
		
		LeftBottomContainer.setBackground(Color.decode(DARK_BLUE));
		LeftBottomContainer.setLayout(new BoxLayout(LeftBottomContainer, BoxLayout.Y_AXIS));
		LeftBottomContainer.setPreferredSize(new Dimension(180, 320));
			ProfilePictureLabel = new JLabel(getSpecificImageSize("profile-pic.png", 90, 90, false));
			ProfilePictureLabel.setPreferredSize(new Dimension(90, 90));
			ProfilePictureLabel.setBorder(new EmptyBorder(100, 0, 10, 0));
			ProfilePictureLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			
			UsernameLabel = new JLabel("Ali Hussain");
			UsernameLabel.setFont(_BOLD_16);
			UsernameLabel.setForeground(Color.WHITE);
			UsernameLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			
			EditProfileLabel = new JLabel("Edit Profile");
			EditProfileLabel.setFont(getCustomFont("SemiBold", 12));
			EditProfileLabel.setForeground(Color.WHITE);
			EditProfileLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		LeftBottomContainer.add(ProfilePictureLabel);
		LeftBottomContainer.add(UsernameLabel);
		LeftBottomContainer.add(EditProfileLabel);
		
		// Right panel
		RightContainer.setBackground(Color.decode(DARK_BLUE));
		RightContainer.add(RoundedPanel, BorderLayout.CENTER);
		RightContainer.add(Box.createRigidArea(new Dimension(0, 20)), BorderLayout.NORTH);
		RightContainer.add(Box.createRigidArea(new Dimension(0, 20)), BorderLayout.SOUTH);
		RightContainer.add(Box.createRigidArea(new Dimension(20, 0)), BorderLayout.WEST);
		RightContainer.add(Box.createRigidArea(new Dimension(20, 0)), BorderLayout.EAST);
		RoundedPanel.add(MainPanel);
		
		TargetPanel.setBackground(Color.decode(DARK_BLUE_2));
		TargetPanel.setLayout(null);
		
			JLabel PreviewImageLabel = new JLabel(getSpecificImageSize("PIcons/iPhoneX.png", 120, 240, false));
				PreviewImageLabel.setBounds(60, 40, 120, 240);
			TargetPanel.add(PreviewImageLabel);
			
			JLabel ProductNameLabel = new JLabel("Apple iPhone X With FaceTime (246 GB, Silver)");
				ProductNameLabel.setBounds(210, 60, 500, 30);
				ProductNameLabel.setForeground(Color.WHITE);
				ProductNameLabel.setFont(getCustomFont("Bold", 20));
			TargetPanel.add(ProductNameLabel);
			
			JLabel ProductPriceLabel = new JLabel("$999.0");
				ProductPriceLabel.setBounds(210, 110, 100, 30);
				ProductPriceLabel.setForeground(Color.WHITE);
				ProductPriceLabel.setFont(getCustomFont("Bold", 24));
			TargetPanel.add(ProductPriceLabel);
			
			JLabel ProductSellerName = new JLabel("Apple");
				ProductSellerName.setBounds(210, 150, 100, 30);
				ProductSellerName.setForeground(Color.decode(LIGHT_BLUE));
				ProductSellerName.setFont(getCustomFont("Bold", 16));
			TargetPanel.add(ProductSellerName);
			
			JLabel ProductRating = new JLabel(getSpecificImageSize("PIcons/five-star.png", 0, 0, true));
				ProductRating.setBounds(290, 150, 130, 30);
			TargetPanel.add(ProductRating);
			
			JLabel ProductVotes = new JLabel("15 Vote(s)");
				ProductVotes.setBounds(430, 150, 100, 30);
				ProductVotes.setForeground(Color.decode(LIGHT_BLUE));
				ProductVotes.setFont(getCustomFont("Bold", 12));
			TargetPanel.add(ProductVotes);
			
			JLabel ProductDesciption = new JLabel("<html><p><i>Our vision has always been to create an iPhone that is entirely screen. One so immersive the device itself disappears into the experience.");
				ProductDesciption.setBounds(200, 180, 650, 100);
				ProductDesciption.setForeground(Color.WHITE);
				ProductDesciption.setFont(getCustomFont("Bold", 14));
			TargetPanel.add(ProductDesciption);
			
			JRadioButton CreditCardRadioButton = new JRadioButton("Credit Card");
				CreditCardRadioButton.setBounds(210, 280, 150, 30);
				CreditCardRadioButton.setForeground(Color.WHITE);
				CreditCardRadioButton.setFont(getCustomFont("Bold", 18));
			TargetPanel.add(CreditCardRadioButton);
			
			JRadioButton CashOnDeliveryRadioButton = new JRadioButton("Cash on Delivery");
				CashOnDeliveryRadioButton.setBounds(400, 280, 200, 30);
				CashOnDeliveryRadioButton.setForeground(Color.WHITE);
				CashOnDeliveryRadioButton.setFont(getCustomFont("Bold", 18));
			TargetPanel.add(CashOnDeliveryRadioButton);
			
			ButtonGroup buttonGroup = new ButtonGroup();
				buttonGroup.add(CreditCardRadioButton);
				buttonGroup.add(CashOnDeliveryRadioButton);
				
			CardNumberLabel = new JLabel("Card Number");
				CardNumberLabel.setBounds(215, 330, 100, 30);
				CardNumberLabel.setForeground(Color.WHITE);
				CardNumberLabel.setFont(getCustomFont("Bold", 12));
			TargetPanel.add(CardNumberLabel);
			
			CardNumberTextField = new JTextField(20);
				CardNumberTextField.setBounds(210, 355, 400, 30);
				CardNumberTextField.setBackground(Color.decode(DARK_BLUE));
				CardNumberTextField.setForeground(Color.WHITE);
				CardNumberTextField.setCaretColor(Color.WHITE);
				CardNumberTextField.setFont(getCustomFont("Medium", 18));
			TargetPanel.add(CardNumberTextField);
			
			CVCLabel = new JLabel("CVC");
				CVCLabel.setBounds(215, 390, 100, 30);
				CVCLabel.setForeground(Color.WHITE);
				CVCLabel.setFont(getCustomFont("Bold", 12));
			TargetPanel.add(CVCLabel);
			
			CVCTextField = new JTextField(20);
				CVCTextField.setBounds(210, 415, 400, 30);
				CVCTextField.setBackground(Color.decode(DARK_BLUE));
				CVCTextField.setForeground(Color.WHITE);
				CVCTextField.setCaretColor(Color.WHITE);
				CVCTextField.setFont(getCustomFont("Medium", 18));
			TargetPanel.add(CVCTextField);
			
			AddToCartButton = new JButton("Add to Cart");
				AddToCartButton.setBounds(215, 580, 150, 40);
				AddToCartButton.setForeground(Color.WHITE);
				AddToCartButton.setBackground(Color.decode(DARK_BLUE));
				AddToCartButton.setFont(getCustomFont("Bold", 18));
			TargetPanel.add(AddToCartButton);
			
			BuyNowButton = new JButton("Buy Now");
				BuyNowButton.setBounds(400, 580, 150, 40);
				BuyNowButton.setForeground(Color.WHITE);
				BuyNowButton.setBackground(Color.decode("#FFCE39"));
				BuyNowButton.setFont(getCustomFont("Bold", 18));
			TargetPanel.add(BuyNowButton);
			
			CVCLabel.setVisible(false);
			CVCTextField.setVisible(false);
	
			CardNumberLabel.setVisible(false);
			CardNumberTextField.setVisible(false);
		
		MainPanel.add(TargetPanel, BorderLayout.CENTER);
		
		CreditCardRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CVCLabel.setVisible(true);
				CVCTextField.setVisible(true);
			
				CardNumberLabel.setVisible(true);
				CardNumberTextField.setVisible(true);
			}
		});
		
		CashOnDeliveryRadioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CVCLabel.setVisible(false);
				CVCTextField.setVisible(false);
			
				CardNumberLabel.setVisible(false);
				CardNumberTextField.setVisible(false);
			}
		});
		
		MainPanel.setPreferredSize(new Dimension(860, 637));
		MainPanel.setBackground(Color.decode(DARK_BLUE_2));
		
		// Add components
		LeftContainer.add(LeftTopContainer, BorderLayout.NORTH);
		LeftContainer.add(LeftBottomContainer, BorderLayout.SOUTH);
		
		add(TitleContainer, BorderLayout.NORTH);
		add(RightContainer,  BorderLayout.CENTER);
		add(LeftContainer,  BorderLayout.WEST);
	
	}
	
	private ImageIcon getSpecificImageSize(String imageString, int width, int height, boolean defaultSize) {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("Images/" + imageString));
		Image image = imageIcon.getImage();
		if (defaultSize) {
			return new ImageIcon(image);
		} else {
			Image newImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			return new ImageIcon(newImage);
		}
	}
	
	private Font getCustomFont(String fontName, int size) {
		try {
			return Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("Fonts/"+ fontName + ".ttf")).deriveFont(Font.PLAIN, size);
		} catch (FontFormatException | IOException ex) {
			System.err.println("FONT EXCEPTION: \n" + ex);
		}
		return new Font(null, Font.PLAIN, 3);
	}

	@Override
	public String getTitle() {
		return HeaderTitleLabel.getText();
	}
	
	@Override
	public void setTitle(String newTitle) {
		HeaderTitleLabel.setText(newTitle);
	}
	
}
