package com.alateiyah.proj;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Ali Hussain
 */

public class Home extends JFrame {
	
	// Properties
	private final static String DARK_BLUE = "#34374C";
	private final static String DARK_BLUE_2 = "#2C2E3E";
	private final static String LIGHT_BLUE = "#5D6074";

	// Components
	private static JPanel TitleContainer, RightContainer, LeftContainer, LeftTopContainer, LeftBottomContainer, RoundedPanel, MainPanel, HomeTargetPanel;
	private static JLabel LogoIconLabel, HomeLabel, SearchLabel, CartLabel, OrdersLabel, HelpLabel, AboutLabel;
	private static JLabel ProfilePictureLabel, UsernameLabel, EditProfileLabel;
	private static JLabel HeaderTitleLabel, ExitIcon, EmptyLabel;
	private static JNTable HomeTableOfProducts;
	private static JLabel SmartphoneLabel, LaptopLabel, DesktopLabel, TelevisonLabel, PrinterLabel, AccessoryLabel;

	// Fonts
	private static Font _BOLD_16;

	// Constructor
	public Home() throws IOException {
		// Definition
		TitleContainer = new JPanel(new BorderLayout(100, 100));
		RightContainer = new JPanel(new BorderLayout());
			MainPanel = new JPanel(new BorderLayout());
				HomeTargetPanel = new JPanel();
			
        RoundedPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Insets insets = getInsets();
                int x = insets.left;
                int y = insets.top;
                int w = getWidth() - insets.left - insets.right;
                int h = getHeight() - insets.top - insets.bottom;
                int arc = 25;

                Graphics2D g2 = (Graphics2D) g.create();
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setColor(Color.decode(DARK_BLUE_2));
                g2.fillRoundRect(x, y, w, h, arc, arc);
                g2.drawRoundRect(x, y, w, h, arc, arc);
                g2.dispose();
            }

        };
		LeftContainer = new JPanel(new BorderLayout());
			LeftTopContainer = new JPanel();
			LeftBottomContainer = new JPanel();
		
		_BOLD_16 = getCustomFont("Bold", 16);
		
		// Customization
		setLayout(new BorderLayout());
		getContentPane().setBackground(Color.decode(DARK_BLUE));
		
		// Title bar
		TitleContainer.setSize(1090, 30);
		TitleContainer.setBackground(Color.decode(DARK_BLUE_2));
			HeaderTitleLabel = new JLabel("HOME");
			HeaderTitleLabel.setFont(getCustomFont("Bold", 15));
			HeaderTitleLabel.setForeground(Color.WHITE);
			HeaderTitleLabel.setHorizontalAlignment(JLabel.CENTER);
			
			ExitIcon = new JLabel(getSpecificImageSize("X.png", 12, 12, false));
			ExitIcon.setPreferredSize(new Dimension(50, 20));
			
			EmptyLabel = new JLabel();
			EmptyBorder headerWhiteSpace = new EmptyBorder(0, 40, 0, 40);
			EmptyLabel.setBorder(headerWhiteSpace);
		TitleContainer.add(ExitIcon, BorderLayout.WEST);
		TitleContainer.add(HeaderTitleLabel, BorderLayout.CENTER);
		TitleContainer.add(EmptyLabel, BorderLayout.EAST);
		
		// Side menu
		LeftTopContainer.setBackground(Color.decode(DARK_BLUE));
		LeftTopContainer.setLayout(new BoxLayout(LeftTopContainer, BoxLayout.Y_AXIS));
		LeftTopContainer.setPreferredSize(new Dimension(180, 460));
		EmptyBorder leftContainerBorder = new EmptyBorder(25, 50, 0, 0);
		LeftTopContainer.setBorder(leftContainerBorder);
			LogoIconLabel = new JLabel(getSpecificImageSize("loop-logo.png", 80, 80, false));
			LogoIconLabel.setPreferredSize(new Dimension(80, 80));
			EmptyBorder logoWhiteSpace = new EmptyBorder(0, 0, 50, 0);
			LogoIconLabel.setBorder(logoWhiteSpace);
			
			HomeLabel = new JLabel("Home");
			HomeLabel.setFont(_BOLD_16);
			HomeLabel.setForeground(Color.WHITE);
			SearchLabel = new JLabel("Search");
			SearchLabel.setFont(_BOLD_16);
			SearchLabel.setForeground(Color.WHITE);
			
			CartLabel = new JLabel("Cart");
			CartLabel.setFont(_BOLD_16);
			CartLabel.setForeground(Color.WHITE);
			
			OrdersLabel = new JLabel("Orders");
			OrdersLabel.setFont(_BOLD_16);
			OrdersLabel.setForeground(Color.WHITE);
			
			
			AboutLabel = new JLabel("About");
			AboutLabel.setFont(_BOLD_16);
			AboutLabel.setForeground(Color.WHITE);
			
			HelpLabel = new JLabel("Help");
			HelpLabel.setFont(_BOLD_16);
			HelpLabel.setForeground(Color.WHITE);
		LeftTopContainer.add(LogoIconLabel);
		LeftTopContainer.add(HomeLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(SearchLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(CartLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(OrdersLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 50)));
		LeftTopContainer.add(AboutLabel);
		LeftTopContainer.add(Box.createRigidArea(new Dimension(0, 8)));
		LeftTopContainer.add(HelpLabel);
		
		LeftBottomContainer.setBackground(Color.decode(DARK_BLUE));
		LeftBottomContainer.setLayout(new BoxLayout(LeftBottomContainer, BoxLayout.Y_AXIS));
		LeftBottomContainer.setPreferredSize(new Dimension(180, 320));
			ProfilePictureLabel = new JLabel(getSpecificImageSize("profile-pic.png", 90, 90, false));
			ProfilePictureLabel.setPreferredSize(new Dimension(90, 90));
			ProfilePictureLabel.setBorder(new EmptyBorder(100, 0, 10, 0));
			ProfilePictureLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			
			UsernameLabel = new JLabel("Ali Hussain");
			UsernameLabel.setFont(_BOLD_16);
			UsernameLabel.setForeground(Color.WHITE);
			UsernameLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			
			EditProfileLabel = new JLabel("Edit Profile");
			EditProfileLabel.setFont(getCustomFont("SemiBold", 12));
			EditProfileLabel.setForeground(Color.WHITE);
			EditProfileLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		LeftBottomContainer.add(ProfilePictureLabel);
		LeftBottomContainer.add(UsernameLabel);
		LeftBottomContainer.add(EditProfileLabel);
		
		JPanel RightTopContainer = new JPanel(null);
			SmartphoneLabel = new JLabel("Smartphone");
				SmartphoneLabel.setBounds(0, 0, 100, 30);
				SmartphoneLabel.setForeground(Color.WHITE);
				SmartphoneLabel.setFont(_BOLD_16);
			RightTopContainer.add(SmartphoneLabel);
			
			LaptopLabel = new JLabel("Laptop");
				LaptopLabel.setForeground(Color.WHITE);
				LaptopLabel.setFont(_BOLD_16);
			RightTopContainer.add(LaptopLabel);
			
			DesktopLabel = new JLabel("Desktop");
				DesktopLabel.setForeground(Color.WHITE);
				DesktopLabel.setFont(_BOLD_16);
			RightTopContainer.add(DesktopLabel);
			
			TelevisonLabel = new JLabel("Television");
				TelevisonLabel.setForeground(Color.WHITE);
				TelevisonLabel.setFont(_BOLD_16);
			RightTopContainer.add(TelevisonLabel);
			
			PrinterLabel = new JLabel("Printer");
				PrinterLabel.setForeground(Color.WHITE);
				PrinterLabel.setFont(_BOLD_16);
			RightTopContainer.add(PrinterLabel);
			
			AccessoryLabel = new JLabel("Accessory");
				AccessoryLabel.setForeground(Color.WHITE);
				AccessoryLabel.setFont(_BOLD_16);
			RightTopContainer.add(AccessoryLabel);
			RightTopContainer.setBackground(Color.decode(DARK_BLUE));
		// Right panel
		RightContainer.setBackground(Color.decode(DARK_BLUE));
		RightContainer.add(RoundedPanel, BorderLayout.CENTER);
		RightContainer.add(RightTopContainer, BorderLayout.NORTH);
		RightContainer.add(Box.createRigidArea(new Dimension(0, 20)), BorderLayout.SOUTH);
		RightContainer.add(Box.createRigidArea(new Dimension(20, 0)), BorderLayout.WEST);
		RightContainer.add(Box.createRigidArea(new Dimension(20, 0)), BorderLayout.EAST);
		RoundedPanel.add(MainPanel);
		
		MainPanel.add(HomeTargetPanel, BorderLayout.CENTER);
		HomeTargetPanel.setLayout(new BoxLayout(HomeTargetPanel, BoxLayout.Y_AXIS));
		
		MainPanel.setPreferredSize(new Dimension(860, 637));
		MainPanel.setBackground(Color.decode(DARK_BLUE_2));
		HomeTargetPanel.setBackground(Color.decode(DARK_BLUE_2));
		int i = 0;
		HomeTableOfProducts = new JNTable(HomeTargetPanel);
		String desc = "<html><p><i>Our vision has always been to create an iPhone that is entirely screen. One so immersive the device itself disappears into the experience.</html>";
		for (; i < 10; i++)
			HomeTableOfProducts.addNewRow(HomeTableOfProducts.new Item("" + i, "Apple", desc, "PIcons/five-star.png", i, "PIcons/iPhoneX.png"));
		
		MainPanel.add(HomeTableOfProducts.MainScrollPane);
		
		// Action Listeners
		ButtonHandler handler = new ButtonHandler();
		ExitIcon.addMouseListener(handler);
		HomeLabel.addMouseListener(handler);
		SearchLabel.addMouseListener(handler);
		CartLabel.addMouseListener(handler);
		OrdersLabel.addMouseListener(handler);
		AboutLabel.addMouseListener(handler);
		HelpLabel.addMouseListener(handler);
		HeaderTitleLabel.addMouseListener(handler);
		
		// Add components
		LeftContainer.add(LeftTopContainer, BorderLayout.NORTH);
		LeftContainer.add(LeftBottomContainer, BorderLayout.SOUTH);
		
		add(TitleContainer, BorderLayout.NORTH);
		add(RightContainer,  BorderLayout.CENTER);
		add(LeftContainer,  BorderLayout.WEST);
	}
	
	private class ButtonHandler extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent event) {
			if (event.getSource() == HomeLabel) {
				setTitle("HOME");
			} else if (event.getSource() == SearchLabel) {
				setTitle("SEARCH");
			} else if (event.getSource() == CartLabel) {
				setTitle("CART");
			} else if (event.getSource() == OrdersLabel) {
				setTitle("ORDERS");
			} else if (event.getSource() == AboutLabel) {
				setTitle("ABOUT");
			} else if (event.getSource() == HelpLabel) {
				setTitle("HELP");
			} else if (event.getSource() == ExitIcon) {
				System.exit(0);
			} else if (event.getSource() == HeaderTitleLabel) {
				HomeTableOfProducts.removeRow("1");
			}
		}
		
	}
	
	private void setComponentsVisibilty(boolean visibility) {
		for (Component component: MainPanel.getComponents()) {
			component.setVisible(visibility);
		}
	}
	
	private ImageIcon getSpecificImageSize(String imageString, int width, int height, boolean defaultSize) {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("Images/" + imageString));
		Image image = imageIcon.getImage();
		if (defaultSize) {
			return new ImageIcon(image);
		} else {
			Image newImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			return new ImageIcon(newImage);
		}
	}
	
	private Font getCustomFont(String fontName, int size) {
		try {
			return Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("Fonts/"+ fontName + ".ttf")).deriveFont(Font.PLAIN, size);
		} catch (FontFormatException | IOException ex) {
			System.err.println("FONT EXCEPTION: \n" + ex);
		}
		return new Font(null, Font.PLAIN, 8);
	}

	public String getTitle() {
		return HeaderTitleLabel.getText();
	}
	
	public void setTitle(String newTitle) {
		HeaderTitleLabel.setText(newTitle);
	}
	
}